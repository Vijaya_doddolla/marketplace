package com.atlassian.marketplace.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "contact")
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties
public class Contact {

    @Id
    private String name;
    @Column(name = "email_address")
    private String emailAddress;
    @Column(name = "address_line_1")
    private String addressLine1;
    @Column(name = "address_line_2")
    private String addressLine2;
    private String city;
    private String state;
    @Column(name = "postal_code")
    private int postalCode;
    private String country;
    @Column(name = "company_name")
    private String companyName;
}
