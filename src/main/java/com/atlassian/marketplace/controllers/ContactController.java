package com.atlassian.marketplace.controllers;

import com.atlassian.marketplace.model.Contact;
import com.atlassian.marketplace.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ContactController {

    @Autowired
    private ContactRepository contactRepository;

    @GetMapping("/contacts")
    public List<Contact> getAllContacts() {
        return contactRepository.findAll();
    }

    @GetMapping("/contact/{name}")
    public Optional<Contact> getContact(@PathVariable String name) {
        return contactRepository.findById(name);
    }

    @GetMapping("/account/contacts/{companyname}")
    public List<Contact> getAllContactsByAccountName(@PathVariable String companyname) {
        return contactRepository.findByCompanyName(companyname);
    }

    @PostMapping("/contact")
    public ResponseEntity<String> createContact(@RequestBody Contact contact) {
        contactRepository.save(contact);
        return ResponseEntity.ok("success");
    }

    @PutMapping("/contact/{name}")
    public ResponseEntity<String> updateContact(@PathVariable String name,
                                                @RequestBody Contact contactDetails) {
        try {
            Contact contact = contactRepository.findById(name)
                    .orElseThrow(Exception::new);
            contact.setAddressLine1(contactDetails.getAddressLine1());
            contact.setAddressLine2(contactDetails.getAddressLine2());
            contact.setCity(contactDetails.getCity());
            contact.setState(contactDetails.getState());
            contact.setPostalCode(contactDetails.getPostalCode());
            contact.setCountry(contactDetails.getCountry());
            contact.setCompanyName(contactDetails.getCompanyName());

            contactRepository.save(contact);
        } catch (Exception e) {
            e.printStackTrace();
            ResponseEntity.badRequest();
        }
        return ResponseEntity.ok("success");
    }

    @DeleteMapping("/contact/{name}")
    public ResponseEntity<String> deleteContact(@PathVariable String name) throws Exception {
        Contact contact = contactRepository.findById(name).orElseThrow(Exception::new);
        contactRepository.delete(contact);
        return ResponseEntity.ok("success");
    }
}