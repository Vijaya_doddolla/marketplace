package com.atlassian.marketplace.controllers;

import com.atlassian.marketplace.model.Account;
import com.atlassian.marketplace.repository.AccountRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class AccountController {

    private final AccountRepository accountRepository;

    public AccountController(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @GetMapping("/accounts")
    public List<Account> getAllAccounts() {
        return accountRepository.findAll();
    }

    @GetMapping("/account/{name}")
    public Optional<Account> getAccount(@PathVariable String name) {
        return accountRepository.findById(name);
    }


    @PostMapping("/account")
    public ResponseEntity<String> createAccount(@RequestBody Account account) {
        accountRepository.save(account);
        return ResponseEntity.ok("success");
    }

    @PutMapping("/account/{name}")
    public ResponseEntity<String> updateAccount(@PathVariable String name,
                                                @RequestBody Account accountDetails) throws Exception {

        Account account = accountRepository.findById(name)
                .orElseThrow(() -> new Exception("Account name: " + name + "not found "));

        account.setAddressLine1(accountDetails.getAddressLine1());
        account.setAddressLine2(accountDetails.getAddressLine2());
        account.setCity(accountDetails.getCity());
        account.setState(accountDetails.getState());
        account.setPostalCode(accountDetails.getPostalCode());
        account.setCountry(accountDetails.getCountry());

        accountRepository.save(account);

        return ResponseEntity.ok("success");
    }

    @DeleteMapping("/account/{name}")
    public ResponseEntity<String> deleteAccount(@PathVariable String name) throws Exception {
        Account account = accountRepository.findById(name).orElseThrow(Exception::new);
        accountRepository.delete(account);
        return ResponseEntity.ok("success");
    }

}