package com.atlassian.marketplace.repository;

import com.atlassian.marketplace.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Serializable> {
    List<Contact> findByCompanyName(String companyName);
}