create
database marketplace;

use
marketplace;

create table account
(
    company_name   VARCHAR(50) NOT NULL PRIMARY KEY,
    address_line_1 VARCHAR(50),
    address_line_2 VARCHAR(50),
    city           VARCHAR(20),
    state          VARCHAR(20),
    postal_code    INT,
    country        VARCHAR(20)
);

create table contact
(
    name           VARCHAR(50) NOT NULL PRIMARY KEY,
    email_address  VARCHAR(50),
    address_line_1 VARCHAR(50),
    address_line_2 VARCHAR(50),
    city           VARCHAR(20),
    state          VARCHAR(20),
    postal_code    INT,
    country        VARCHAR(20),
    company_name   VARCHAR(50),
    FOREIGN KEY (company_name)
        REFERENCES account (company_name)
        ON UPDATE RESTRICT
        ON DELETE SET NULL
);