package com.atlassian.marketplace.controllers;

import com.atlassian.marketplace.model.Contact;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static com.atlassian.marketplace.controllers.AccountControllerTest.asJsonString;
import static java.util.Collections.singletonList;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ContactController.class)
public class ContactControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ContactController contactController;

    @Test
    public void getContacts() throws Exception {
        Contact contact = new Contact();
        contact.setName("JAMES");

        List<Contact> allContacts = singletonList(contact);

        given(contactController.getAllContacts()).willReturn(allContacts);

        mvc.perform(get("/api/v1/contacts")
                        .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(contact.getName())));
    }

    @Test
    public void getContact() throws Exception {
        Contact contact = new Contact();
        contact.setName("JAMES");

        given(contactController.getContact(contact.getName())).willReturn(java.util.Optional.of(contact));

        mvc.perform(get("/api/v1/contact/JAMES")
                        .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(contact.getName())));
    }

    @Test
    public void createContact() throws Exception {
        Contact contact = new Contact();
        contact.setName("ABC");


        mvc.perform(post("/api/v1/contact")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(contact)))

                // Validate the response code
                .andExpect(status().isOk());
    }

    @Test
    public void updateContact() throws Exception {
        Contact contact = new Contact();
        contact.setName("ABC");
        contact.setCity("Pleasanton");


        mvc.perform(put("/api/v1/contact/ABC")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(contact)))

                // Validate the response code
                .andExpect(status().isOk());
    }

}
