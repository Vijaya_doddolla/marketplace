package com.atlassian.marketplace.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class SmokeTest {

    @Autowired
    private AccountController accountController;

    @Test
    public void accountContextLoads() throws Exception {
        assertThat(accountController).isNotNull();
    }

    @Autowired
    private ContactController contactController;

    @Test
    public void contactContextLoads() throws Exception {
        assertThat(contactController).isNotNull();
    }

}