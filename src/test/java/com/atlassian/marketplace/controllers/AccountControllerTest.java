package com.atlassian.marketplace.controllers;

import com.atlassian.marketplace.model.Account;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AccountController.class)
public class AccountControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AccountController accountController;

    @Test
    public void getAccounts() throws Exception {
        Account account = new Account();
        account.setCompanyName("ABC");

        List<Account> allAccounts = singletonList(account);

        given(accountController.getAllAccounts()).willReturn(allAccounts);

        mvc.perform(get("/api/v1/accounts")
                        .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].companyName", is(account.getCompanyName())));
    }

    @Test
    public void getAccount() throws Exception {
        Account account = new Account();
        account.setCompanyName("ABC");

        given(accountController.getAccount(account.getCompanyName())).willReturn(java.util.Optional.of(account));

        mvc.perform(get("/api/v1/account/ABC")
                        .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.companyName", is(account.getCompanyName())));
    }

    @Test
    public void createAccount() throws Exception {
        Account account = new Account();
        account.setCompanyName("ABC");


        mvc.perform(post("/api/v1/account")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(account)))

                // Validate the response code
                .andExpect(status().isOk());
    }

    @Test
    public void updateAccount() throws Exception {
        Account account = new Account();
        account.setCompanyName("ABC");


        mvc.perform(put("/api/v1/account/ABC")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(account)))

                // Validate the response code
                .andExpect(status().isOk());
    }


    static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
