package com.atlassian.marketplace.repository;

import com.atlassian.marketplace.model.Contact;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ContactRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ContactRepository contactRepository;

    @Test
    public void whenFindAll() {
        //given
        Contact contact = new Contact();
        contact.setName("ABC");
        entityManager.persistAndFlush(contact);

        //when
        List<Contact> contacts = contactRepository.findAll();

        //then
        assert (contacts.get(0).getName().equals(contact.getName()));
    }
}
