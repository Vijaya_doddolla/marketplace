package com.atlassian.marketplace.repository;

import com.atlassian.marketplace.model.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class AccountRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void whenFindAll() {
        //given
        Account account1 = new Account();
        account1.setCompanyName("Abc");
        entityManager.persistAndFlush(account1);

        Account account2 = new Account();
        account2.setCompanyName("Def");
        entityManager.persistAndFlush(account2);

        //when
        List<Account> accounts = accountRepository.findAll();

        //then
        assert (accounts.get(0).getCompanyName().equals(account1.getCompanyName()));
        assert (accounts.get(1).getCompanyName().equals(account1.getCompanyName()));
    }


}
