# Marketplace

- Marketplace application service that can store and serve basic information about Marketplace vendor accounts (
  companies who sell apps on marketplace.atlassian.com) and the contacts (people) who are associated with those accounts

### Project Structure and Setup

- Selected spring-boot framework along with spring data jpa to fulfil the goal of this project.
- Created base skeleton project from [Spring.io](https://start.spring.io/)


- Tech Stacks:

  | Stack | Version |
      | ----------- | ----------- |
  | Programing Language | Java 1.8 |
  | Build Tool | Maven |
  | Frameworks | Spring-Boot, Spring Data Jpa, Junit4, Mockito |
  | Database | MySql |
  | Other tools | Intellij, MySql WorkBench, Postman |

### How to build and run project?

- Setup MySql database server locally and create table schemas
  using [marketplace.sql](./src/main/resources/marketplace.sql)
- Run below to download maven dependencies, build and run code locally

  To build:
  ```shell
  $ mvn clean install
  $ mvn compile
  ```
  To run:
  ```shell
  $ mvn spring-boot:run
  ```

### How to test rest api endpoints?
- Included the sample payloads for all rest endpoints at [rest-api](./rest-api)



### TODO List:
###### Authentication & Authorization
Currently, the rest endpoints are widely open and anyone can create and update the account & contact list in marketplace.
We can use spring security or oauth2.0 to achieve this.
###### Validations and Exception handling
There are some validations are covered part of this code,
and more data validations and exception handling needed to keep service more reliable and handle all types of request
without any failures.
###### Set user understandable http response codes and message in api responses.
Every handled error responses should be returned as user understandable format.
ie much like a json object with http status code and error message.
So, this make much easier for client who integrates with these apis.
```json
{"statusCode": 400, "message":"Bad Request. The contact name is mandatory field in api request"}
```
### Feature Proposals:
Marketplace is a huge system that include lot of business components and application services. There are lot actors and 
user get involved in different product flows. There are in a lot of chances to improving the system that serves all the
product sellers and customers.
**Accounts and Contacts** are major data sources for other components below and some are directly linked.
Some major components:
- Product: Catalog, Product information, 
- Pricing: Discounts, Subscriptions, Term Prices.
- Payments: Different payment types
- Licenses: 
- Sales:
- Rating:
- Analytics & Reports
- Searching
- Customer & Tech Support
- Production Communications
- User and Profiles
- etc...

###### We can add some additional feature to accounts & contacts.
- Account status: set status account level `ACTIVE, IN-ACTIVE, TERMINATED, BLOCKED`
This helps to tract the account and direct the customers to right place for shopping.
- Account Pricing Policies: Setting pricing per account that gives more understanding price sharing on products which 
they sell thorough marketplace 

